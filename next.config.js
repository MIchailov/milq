const isProd = process.env.NODE_ENV === 'production'

/** @type {import('next').NextConfig} */
const nextConfig = {
  output: 'export',
  reactStrictMode: true,
  images: {
    unoptimized: true,
  },
  assetPrefix: isProd ? 'https://michailov.gitlab.io/milq/' : '',
}

module.exports = nextConfig
