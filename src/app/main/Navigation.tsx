'use client'

const isProd = process.env.NODE_ENV === 'production'

import s from "@/app/main/Navigation.module.scss";
import Link from "next/link";
import { usePathname } from 'next/navigation'
import colors from '../../styles/colors.module.scss';

const navLinks = [
  { title: 'Home', href: isProd ? '/milq/main' : '/main' },
  { title: 'Contacts', href: isProd ? '/milq/main/contacts' : '/main/contacts' },
  { title: 'About', href: isProd ? '/milq/main/about' : '/main/about'}
]

export default function Navigation() {

  const pathname = usePathname();

  return (
    <div className={s.Menu}>
      { navLinks.map((link, i) => (
        <div key={i}>
          <Link href={link.href}>
            <span style={{ color: `${pathname === link.href ? colors['colorTextLink'] : 'inherit'}` }}>
              {link.title}
            </span>
          </Link>
        </div>
      ))}
    </div>
  )
}
