import s from './layout.module.scss';
import Navigation from './Navigation';
import { ScrambledText } from "@/uikit";

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <>
      <div className={'container'}>
        <header className={s.Header}>
          <div className={s.Title}>
            <div className={s.Description}>D. Mykhailov. Seasoned UX designer</div>
            <ScrambledText
              slideLength={2000}
              value={[
                '[ Product design ]',
                '[ Prototyping ]',
                '[ Infographic ]',
                '[ Design systems ]',
                '[ React/Angular components ]',
                '[ Business and system analytics ]',
              ]}
            />
          </div>
          <Navigation />
        </header>
      </div>

      <div>
        {children}
      </div>

    </>

  )
}
