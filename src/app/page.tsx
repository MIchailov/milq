'use client'

const isProd = process.env.NODE_ENV === 'production'

import { redirect } from 'next/navigation'

export default function Home() {

  if (isProd) {
    redirect('/milq/main');
  } else {
    redirect('/main');
  }


  return (
    <main className='container'>
      Redirect to main page
    </main>
  )
}
